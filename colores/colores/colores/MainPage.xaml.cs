﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace colores
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        async private void ButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());
        }

        void OnSliderValueChanged(object sender, ValueChangedEventArgs args)
        {

            id_bv_background.BackgroundColor = (Color.FromRgba((int)id_sl_red.Value, (int)id_sl_green.Value, (int)id_sl_Blue.Value, (int)id_sl_alpha.Value));

            if (sender == id_sl_red)
            {
                id_lb_red.Text = id_sl_red.Value.ToString();
            }
            else if (sender == id_sl_green)
            {
                id_lb_green.Text = id_sl_green.Value.ToString();
            }
            else if (sender == id_sl_Blue)
            {
                id_lb_blue.Text = id_sl_Blue.Value.ToString();
            }
            else if (sender == id_sl_alpha)
            {
                id_lb_alpha.Text = id_sl_alpha.Value.ToString();
            }


        }
	}
}
