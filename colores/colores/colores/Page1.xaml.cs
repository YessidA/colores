﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace colores
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
		public Page1 ()
		{
			InitializeComponent ();
		}

        async private void ButtonClickedR(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
        }

        void OnSliderValueChanged(object sender, ValueChangedEventArgs args)
        {
            
            if (sender == id_sl_width)
            {
                id_box_view.WidthRequest = (int)id_sl_width.Value;
                id_box_view.HeightRequest = (int)id_sl_width.Value;

                id_sl_high.Value = id_sl_width.Value;

                id_lb_width.Text = id_sl_width.Value.ToString();
                id_lb_high.Text = id_sl_width.Value.ToString();
            }

            if (sender == id_sl_high)
            {
                id_box_view.WidthRequest = (int)id_sl_high.Value;
                id_box_view.HeightRequest = (int)id_sl_high.Value;

                id_sl_width.Value = id_sl_high.Value;

                id_lb_width.Text = id_sl_high.Value.ToString();
                id_lb_high.Text = id_sl_high.Value.ToString();
            }
        }


    }
}